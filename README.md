*********************************************************************************************
Use of the Different methodologies for PLC programming. (IEC 61131-3, Beckhoff TwinCAT3, HMI)
*********************************************************************************************

*********************************************************************************************
Flowchart:
*********************************************************************************************
The Flowchart is given below (Figure 1) used to develop the program(also a video and the
programs printouts are attached ).
Description:
1. The system first initiates with the “start” after that the conveyor runs and get output from the conveyor motor, c1_m until it detects signal from the Sensor of the conveyor, c1_s. When the c1_s activated, the workpiece is ready to pick up.
2. The checks the r_mnb to determine robot status and r_ba1 and r_ba2 signal tell the robot where to deliver the piece. Here r_ba1 signal is given priority over r_ba2 signal.
3. Depending upon the m1_s* signal the workpiece is transferred and to provide an acknowledgment and to start the same procedure ‘Transferred’ signal is used. Initial m1_s and m2_s are kept at logical 0.

From Flowchart to the program:
For creating ladder logic, TwinCAT software is used. To develop the program, flowing steps are used:
1. for flowchart:
• Enumerate functions
• Enumerate transitions
2. for Ladder Logic
• Transition Logic
• Function logic
• Output logic


*************************************************************************************************
Statechart:
*************************************************************************************************
The statechart is given below(Figure 3) used to develop the program(also a video and the
programs printouts are attached ).
Description:
4. The system first initiates with the “start” after that the conveyor runs and get output from the conveyor motor, c1_m until it detects signal from the Sensor of the
conveyor, c1_s. When the c1_s activated, the workpiece is ready to pick up.
5. The checks the r_mnb to determine robot status and r_ba1 and r_ba2 signal tell the robot where to deliver the piece.
6. Depending upon the m1_s* signal the workpiece is transferred and to provide an acknowledgment and to start the same procedure Transferred signal is used. 


From state chart to the program:
Building the state chart
1. According to the above description, six states can be found.
2. Then the output of each state are determined
3. Then the transition between states are determined Writing the ladder logic
1. For creating ladder logic, TwinCAT software is used. After declaring required variables, in the first network ‘start’ input used to reset all the state without the S1 state.
2. State logics are developed with Set and Reset coil type.
3. Output logics are developed with Normal coil type.


References:
[1]. Lecture slides and TwinCAT Exercise sessions, Andrei Lobov, ASE-9416 (A'17), Discrete Automation Systems, TUT.